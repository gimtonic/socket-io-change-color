import React, { useState, useEffect } from 'react';
import socketIOClient from 'socket.io-client';
import { END_POINT } from './constants';

function App() {
  const [ color, setColor ] = useState('white');

  useEffect(() => {
    const socket = socketIOClient(END_POINT);
    
    setInterval(send(), 1000);
    socket.on('change color', (color) => {
        document.body.style.backgroundColor = color
    })
  }, [])

  const send = () => {
    const socket = socketIOClient(END_POINT);
    socket.emit('change color', color)
  }

  return (
    <div style={{ textAlign: "center" }}>
      <button onClick={() => send() }>Change Color</button>
      <button id="blue" onClick={() => setColor('blue')}>Blue</button>
      <button id="red" onClick={() => setColor('red')}>Red</button>
    </div>
  );
}

export default App;
